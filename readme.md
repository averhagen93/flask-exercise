1. To run the app you'll need poetry installed on your main python installation.
2. If poetry isn't installed, run: pip install poetry
3. Now that poetry is on your path, you need to install the virtualenv. Navigate to the project's base directory and run: poetry install
4. To start the application navigate to the project's base directory and run: poetry run flask run
5. The app should now be running on 127.0.0.1:5000