import time
from flask import Flask, render_template, request
from store_exercise.data import ExerciseData

app = Flask(__name__)
exercise_data = ExerciseData(data_dir="./data")


@app.route("/")
def home():
    return render_template(
        "index.html",
        content={
            "title": "Brands",
            "names": exercise_data.choices,
            "buy_button_text": "Purchase Brands Together",
            "historic_title": "Historic Purchases",
            "historic_groupings": exercise_data.groupings,
        },
    )


@app.route("/", methods=["POST"])
def couple_together():
    selected_stores = list(request.form.listvalues())[0]
    exercise_data.add_grouping(selected_stores)

    return home()
