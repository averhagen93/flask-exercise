import json
import typing
from typing import List
from pathlib import Path


class ExerciseData:
    def __init__(self, data_dir: typing.Union[str, Path]):
        self.data_dir = Path(data_dir)
        self.data_dir.mkdir(parents=True, exist_ok=True)
        self.choices_file = self.data_dir.joinpath("choices.json")
        self.groupings_file = self.data_dir.joinpath("groupings.json")
        self.load_data()
    
    def load_json_file(self, file_to_load: Path, initial_value):
        if not file_to_load.exists():
            with open(file_to_load, "w") as load_file:
                json.dump(initial_value, load_file)
        return json.loads(file_to_load.read_text())
        

    def load_data(self):
        self.choices = self.load_json_file(self.choices_file, [])
        self.groupings = self.load_json_file(self.groupings_file, [])

    def add_grouping(self, grouping: List[str]):
        self.groupings.append(grouping)
        with open(self.groupings_file.absolute(), "w") as output_file:
            json.dump(self.groupings, output_file)
        self.load_data()
